/*
 * Intel Skylake I2S Machine Driver WM8904
 *
 * Copyright (C) 2016, DAQRI, LLC. All rights reserved.
 *
 * Modified from:
 *   Intel Skylake I2S Machine Driver RT826
 *
 *   Copyright (C) 2014-2015, Intel Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/jack.h>
#include <sound/pcm_params.h>
#include "../../codecs/wm8904.h"

#undef USE_DUMMY_CODEC /* don't use wm8904 codec driver. usespace to configure codec over i2c */

/* @TODO should come from ACPI/platform data */
#define SKYLAKE_WM8904_GPIO_HP 441 /* Skylake GPP_D9 */

struct skylake_wm8904_data {
	struct gpio_desc *gpio_spk;
};

#ifndef USE_DUMMY_CODEC
static struct snd_soc_jack skylake_headset;
/* Headset jack detection DAPM pins */
static struct snd_soc_jack_pin skylake_headset_pins[] = {
	{
		.pin = "Mic Jack",
		.mask = SND_JACK_MICROPHONE,
	},
	{
		.pin = "Headphone",
		.mask = SND_JACK_HEADPHONE,
	},
};
static struct snd_soc_jack_gpio headset_jack_gpios[] = {
	{
		.gpio = SKYLAKE_WM8904_GPIO_HP,
		.name = "headphone-gpio",
		.report = SND_JACK_HEADPHONE,
		.invert = 1,
		.debounce_time = 200,
	},
};
#endif

/* turn speaker amplifier on/off depending on use */
static int skylake_spk_event(struct snd_soc_dapm_widget *widget,
			     struct snd_kcontrol *ctrl, int event)
{
	struct snd_soc_card *card = widget->dapm->card;
	struct skylake_wm8904_data *priv = snd_soc_card_get_drvdata(card);

	printk(KERN_DEBUG "%s: event %d\n", __func__, event);

	if (priv->gpio_spk)
		gpiod_set_value_cansleep(priv->gpio_spk, SND_SOC_DAPM_EVENT_ON(event));

	return 0;
}

static const struct snd_kcontrol_new skylake_controls[] = {
	SOC_DAPM_PIN_SWITCH("Speaker"),
	SOC_DAPM_PIN_SWITCH("Headphone"),
	SOC_DAPM_PIN_SWITCH("Mic Jack"),
};

static const struct snd_soc_dapm_widget skylake_widgets[] = {
	SND_SOC_DAPM_HP("Headphone", NULL),
	SND_SOC_DAPM_SPK("Speaker", skylake_spk_event),
	SND_SOC_DAPM_MIC("Mic Jack", NULL),
	SND_SOC_DAPM_MIC("DMIC2", NULL),
	SND_SOC_DAPM_MIC("SoC DMIC", NULL),
};

/* map connections to codec pins */
static const struct snd_soc_dapm_route skylake_wm8904_map[] = {
#ifndef USE_DUMMY_CODEC
	/* Speaker -> amplifier -> codec Line Output */
	{"Speaker", NULL, "LINEOUTL"},
	{"Speaker", NULL, "LINEOUTR"},

	/* headphone */
	{"Headphone", NULL, "HPOUTL"},
	{"Headphone", NULL, "HPOUTR"},

	/* other jacks */
	{"MIC1", NULL, "Mic Jack"},

	/* digital mics */
	{"DMIC1 Pin", NULL, "DMIC2"},
	{"DMic", NULL, "SoC DMIC"},

	/* CODEC BE connections */
	{ "AIF1 Playback", NULL, "ssp0 Tx"},
	{ "ssp0 Tx", NULL, "codec0_out"},
	{ "ssp0 Tx", NULL, "codec1_out"},

	{ "codec0_in", NULL, "ssp0 Rx" },
	{ "codec1_in", NULL, "ssp0 Rx" },
	{ "ssp0 Rx", NULL, "AIF1 Capture" },

	{ "dmic01_hifi", NULL, "DMIC01 Rx" },
	{ "DMIC01 Rx", NULL, "DMIC AIF" },

	{ "hif1", NULL, "iDisp Tx"},
	{ "iDisp Tx", NULL, "iDisp_out"},
#else
	{"Speaker", NULL, "Playback"},
	{"Playback", NULL, "ssp0 Tx"},
	{"ssp0 Tx", NULL, "codec0_out"},
	{"ssp0 Tx", NULL, "codec1_out"},

	{"codec0_in", NULL, "ssp0 Rx"},
	{"codec1_in", NULL, "ssp0 Rx"},
	{"ssp0 Rx", NULL, "Capture"},
	{"Capture", NULL, "Mic Jack"},
#endif
};

static int skylake_wm8904_fe_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_dapm_context *dapm;
	struct snd_soc_component *component = rtd->cpu_dai->component;

	dapm = snd_soc_component_get_dapm(component);
	snd_soc_dapm_ignore_suspend(dapm, "Reference Capture");

	return 0;
}

#ifndef USE_DUMMY_CODEC
static int skylake_wm8904_codec_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_codec *codec = rtd->codec;
	struct snd_soc_card *card = rtd->card;
	struct skylake_wm8904_data *priv = snd_soc_card_get_drvdata(card);
	int ret;

	if (gpio_is_valid(headset_jack_gpios[0].gpio)) {
		ret = snd_soc_card_jack_new(card, "Headphone",
			SND_JACK_HEADPHONE,
			&skylake_headset,
			skylake_headset_pins, ARRAY_SIZE(skylake_headset_pins));
		if (ret) {
			dev_err(card->dev, "snd_soc_card_jack_new returned: %d\n", ret);
			return ret;
		}

		ret = snd_soc_jack_add_gpios(&skylake_headset,
					     ARRAY_SIZE(headset_jack_gpios),
					     headset_jack_gpios);
		if (ret) {
			dev_err(card->dev, "snd_soc_jack_add_gpio returned: %d\n", ret);
			return ret;
		}
	} else {
		dev_err(card->dev, "headphone detect gpio %d invalid\n",
			headset_jack_gpios[0].gpio);
	}

#if 0 /* TODO MIC */
	wm8904_mic_detect(codec, &skylake_headset);
#endif

	snd_soc_dapm_ignore_suspend(&rtd->card->dapm, "SoC DMIC");

	priv->gpio_spk = devm_gpiod_get_optional(codec->dev, "GPIO2", GPIOD_OUT_LOW);
	if (priv->gpio_spk == NULL)
		dev_err(card->dev, "Get codec GPIO2 gpio failed\n");

	return 0;
}
#endif

static unsigned int rates[] = {
	48000,
};

static struct snd_pcm_hw_constraint_list constraints_rates = {
	.count = ARRAY_SIZE(rates),
	.list  = rates,
	.mask = 0,
};

static unsigned int channels[] = {
	2,
};

static struct snd_pcm_hw_constraint_list constraints_channels = {
	.count = ARRAY_SIZE(channels),
	.list = channels,
	.mask = 0,
};

static int skl_fe_startup(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;

	/*
	 * on this platform for PCM device we support,
	 *	48Khz
	 *	stereo
	 *	16 bit audio
	 */

	runtime->hw.channels_max = 2;
	snd_pcm_hw_constraint_list(runtime, 0, SNDRV_PCM_HW_PARAM_CHANNELS,
					   &constraints_channels);

	runtime->hw.formats = SNDRV_PCM_FMTBIT_S16_LE;
	snd_pcm_hw_constraint_msbits(runtime, 0, 16, 16);

	snd_pcm_hw_constraint_list(runtime, 0,
				SNDRV_PCM_HW_PARAM_RATE, &constraints_rates);

	return 0;
}

static const struct snd_soc_ops skylake_wm8904_fe_ops = {
	.startup = skl_fe_startup,
};

static int skylake_ssp0_fixup(struct snd_soc_pcm_runtime *rtd,
			struct snd_pcm_hw_params *params)
{
	struct snd_interval *rate = hw_param_interval(params,
			SNDRV_PCM_HW_PARAM_RATE);
	struct snd_interval *channels = hw_param_interval(params,
						SNDRV_PCM_HW_PARAM_CHANNELS);
	struct snd_mask *fmt = hw_param_mask(params, SNDRV_PCM_HW_PARAM_FORMAT);

	/* The output is 48KHz, stereo, 16bits */
	rate->min = rate->max = 48000;
	channels->min = channels->max = 2;

	/* set SSP0 to 24 bit */
	snd_mask_none(fmt);
	snd_mask_set(fmt, SNDRV_PCM_FORMAT_S24_LE);
	return 0;
}

#ifndef USE_DUMMY_CODEC
static int skylake_wm8904_hw_params(struct snd_pcm_substream *substream,
	struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	int ret;

	ret = snd_soc_dai_set_sysclk(codec_dai, WM8904_CLK_MCLK, 12288000,
		SND_SOC_CLOCK_IN);
	if (ret < 0)
		dev_err(rtd->dev, "set codec sysclk failed: %d\n", ret);

	return ret;
}

static struct snd_soc_ops skylake_wm8904_ops = {
	.hw_params = skylake_wm8904_hw_params,
};
#endif

#if 0 /* TODO DMIC */
static int skylake_dmic_fixup(struct snd_soc_pcm_runtime *rtd,
				struct snd_pcm_hw_params *params)
{
	struct snd_interval *channels = hw_param_interval(params,
						SNDRV_PCM_HW_PARAM_CHANNELS);
	if (params_channels(params) == 2)
		channels->min = channels->max = 2;
	else
		channels->min = channels->max = 4;

	return 0;
}

static unsigned int channels_dmic[] = {
	2, 4,
};

static struct snd_pcm_hw_constraint_list constraints_dmic_channels = {
	.count = ARRAY_SIZE(channels_dmic),
	.list = channels_dmic,
	.mask = 0,
};

static int skylake_dmic_startup(struct snd_pcm_substream *substream)
{
	struct snd_pcm_runtime *runtime = substream->runtime;

	runtime->hw.channels_max = 4;
	snd_pcm_hw_constraint_list(runtime, 0, SNDRV_PCM_HW_PARAM_CHANNELS,
					   &constraints_dmic_channels);

	return snd_pcm_hw_constraint_list(substream->runtime, 0,
			SNDRV_PCM_HW_PARAM_RATE, &constraints_rates);
}

static struct snd_soc_ops skylake_dmic_ops = {
	.startup = skylake_dmic_startup,
};
#endif

/* skylake digital audio interface glue - connects codec <--> CPU */
static struct snd_soc_dai_link skylake_wm8904_dais[] = {
	/* Front End DAI links */
	{
		.name = "Skl Audio Port",
		.stream_name = "Audio",
		.cpu_dai_name = "System Pin",
		.platform_name = "0000:00:1f.3",
		.nonatomic = 1,
		.dynamic = 1,
		.codec_name = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.init = skylake_wm8904_fe_init,
		.trigger = {
			SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST
		},
		.dpcm_playback = 1,
#ifndef USE_DUMMY_CODEC
		.ops = &skylake_wm8904_fe_ops,
#endif
	},
	{
		.name = "Skl Audio Capture Port",
		.stream_name = "Audio Record",
		.cpu_dai_name = "System Pin",
		.platform_name = "0000:00:1f.3",
		.nonatomic = 1,
		.dynamic = 1,
		.codec_name = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.trigger = {
			SND_SOC_DPCM_TRIGGER_POST,
			SND_SOC_DPCM_TRIGGER_POST
		},
		.dpcm_capture = 1,
#ifndef USE_DUMMY_CODEC
		.ops = &skylake_wm8904_fe_ops,
#endif
	},
	{
		.name = "Skl Audio Reference cap",
		.stream_name = "refcap",
		.cpu_dai_name = "Reference Pin",
		.codec_name = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.platform_name = "0000:00:1f.3",
		.init = NULL,
		.dpcm_capture = 1,
		.ignore_suspend = 1,
		.nonatomic = 1,
		.dynamic = 1,
	},
#if 0 /* TODO DMIC */
	{
		.name = "Skl Audio DMIC cap",
		.stream_name = "dmiccap",
		.cpu_dai_name = "DMIC Pin",
		.codec_name = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
		.platform_name = "0000:00:1f.3",
		.init = NULL,
		.dpcm_capture = 1,
		.nonatomic = 1,
		.dynamic = 1,
		.ops = &skylake_dmic_ops,
	},
#endif

	/* Back End DAI links */
	{
		/* SSP0 - Codec */
		.name = "SSP0-Codec",
		.id = 0,
		.cpu_dai_name = "SSP0 Pin",
		.platform_name = "0000:00:1f.3",
		.no_pcm = 1,
#ifndef USE_DUMMY_CODEC
		.codec_name = "i2c-DAQR1045:00",
		.codec_dai_name = "wm8904-hifi",
		.init = skylake_wm8904_codec_init,
#else
		.codec_name = "snd-soc-dummy",
		.codec_dai_name = "snd-soc-dummy-dai",
#endif
		.dai_fmt = SND_SOC_DAIFMT_I2S |
			SND_SOC_DAIFMT_NB_NF |
			SND_SOC_DAIFMT_CBS_CFS,
		.ignore_pmdown_time = 1,
		.be_hw_params_fixup = skylake_ssp0_fixup,
#ifndef USE_DUMMY_CODEC
		.ops = &skylake_wm8904_ops,
#endif
		.dpcm_playback = 1,
		.dpcm_capture = 1,
	},
#if 0 /* TODO DMIC */
	{
		.name = "dmic01",
		.id = 1,
		.cpu_dai_name = "DMIC01 Pin",
		.codec_name = "dmic-codec",
		.codec_dai_name = "dmic-hifi",
		.platform_name = "0000:00:1f.3",
		.be_hw_params_fixup = skylake_dmic_fixup,
		.ignore_suspend = 1,
		.dpcm_capture = 1,
		.no_pcm = 1,
	},
#endif
};

/* skylake audio machine driver for SPT + WM8904 */
static struct snd_soc_card skylake_wm8904 = {
	.name = "skylake-wm8904",
	.owner = THIS_MODULE,
	.dai_link = skylake_wm8904_dais,
	.num_links = ARRAY_SIZE(skylake_wm8904_dais),
	.controls = skylake_controls,
	.num_controls = ARRAY_SIZE(skylake_controls),
	.dapm_widgets = skylake_widgets,
	.num_dapm_widgets = ARRAY_SIZE(skylake_widgets),
	.dapm_routes = skylake_wm8904_map,
	.num_dapm_routes = ARRAY_SIZE(skylake_wm8904_map),
	.fully_routed = true,
};

static int skylake_audio_probe(struct platform_device *pdev)
{
	struct skylake_wm8904_data *priv;

	skylake_wm8904.dev = &pdev->dev;

	priv = devm_kzalloc(skylake_wm8904.dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	snd_soc_card_set_drvdata(&skylake_wm8904, priv);

	return devm_snd_soc_register_card(&pdev->dev, &skylake_wm8904);
}

static int skylake_audio_remove(struct platform_device *pdev)
{
	snd_soc_jack_free_gpios(&skylake_headset,
				ARRAY_SIZE(headset_jack_gpios),
				headset_jack_gpios);

	return 0;
}

static struct platform_driver skylake_audio = {
	.probe = skylake_audio_probe,
	.remove = skylake_audio_remove,
	.driver = {
		.name = "skl_wm8904_i2s",
		.pm = &snd_soc_pm_ops,
	},
};

module_platform_driver(skylake_audio)

MODULE_AUTHOR("Lucas Magasweran <lucas.magasweran@daqri.com>");
MODULE_DESCRIPTION("Intel SST Audio for Skylake WM8904 Machine");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("platform:skl_wm8904_i2s");
