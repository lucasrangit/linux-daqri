/*
 * loki_hwmon.c - hwmon driver for EC sensors on DAQRI devices.
 *
 * Copyright (c) 2018, DAQRI, LLC.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/acpi.h>
#include <linux/hwmon.h>
#include <linux/dmi.h>

/* from Loki EC Ref_ECRAM.h */
#define EC_BAT1_BT 0x08
#define OEM_MAJOR_REV_RAM 0x36
#define OEM_MINOR_REV_RAM 0x37
#define OEM_SUB_REV_RAM 0x38
#define EC_FAN_DUTY 0xe5
#define EC_FAN_RPM 0xe8

#define EC_FAN_RPM_NOT_READY 0xffff

#define LOKI_SYSTEM_FAN_LABEL "System fan"
#define LOKI_SYSTEM_BAT_LABEL "Battery"

static umode_t loki_hwmon_fan_is_visible(const void *_data, u32 attr, int channel)
{
	switch (attr) {
	case hwmon_fan_input:
	case hwmon_fan_fault:
	case hwmon_fan_label:
		return S_IRUGO;
	default:
		return 0;
	}
}

static umode_t loki_hwmon_pwm_is_visible(const void *_data, u32 attr, int channel)
{
	switch (attr) {
	case hwmon_pwm_input:
		return S_IRUGO | S_IWUSR;
	default:
		return 0;
	}
}

static umode_t loki_hwmon_temp_is_visible(const void *_data, u32 attr, int channel)
{
	switch (attr) {
	case hwmon_temp_input:
	case hwmon_temp_max:
	case hwmon_temp_crit:
	case hwmon_temp_label:
		return S_IRUGO;
	default:
		return 0;
	}
}

/* loki EC is big-endian */
static int loki_ec_read16(u8 addr, u16 *value)
{
	u8 val_lb, val_hb;
	int ret;

	ret = ec_read(addr, &val_hb);
	if (ret)
		return ret;
	ret = ec_read(addr + 1, &val_lb);
	if (ret)
		return ret;
	*value = val_lb | (val_hb << 8);

	return 0;
}

static int loki_hwmon_read_pwm(struct device *dev, u32 attr, int channel,
			       long *val)
{
	u8 ec_val;
	int ret;

	switch (attr) {
	case hwmon_pwm_input:
		ret = ec_read(EC_FAN_DUTY, &ec_val);
		if (ret) {
			dev_dbg(dev, "PWM read error %d\n", ret);
			return ret;
		}
		*val = ec_val;
		return 0;
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_read_temp(struct device *dev, u32 attr, int channel,
				long *val)
{
	u16 ec_val;
	int ret;

	switch (attr) {
	case hwmon_temp_input:
		ret = loki_ec_read16(EC_BAT1_BT, &ec_val);
		if (ret) {
			dev_dbg(dev, "Battery temp read error %d\n", ret);
			return ret;
		}
		/* convert decidegree Kelvin to millidegree Celsius */
		*val = ec_val * 100 - 273 * 1000;
		return 0;
	case hwmon_temp_max:
		*val = 58 * 1000;
		return 0;
	case hwmon_temp_crit:
		*val = 60 * 1000;
		return 0;
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_read_fan(struct device *dev, u32 attr, int channel,
			       long *val)
{
	int ret;
	u16 ec_val;

	switch (attr) {
	case hwmon_fan_input: {
		ret = loki_ec_read16(EC_FAN_RPM, &ec_val);
		if (ret)
			return ret;
		if (ec_val == EC_FAN_RPM_NOT_READY) {
			dev_dbg(dev, "RPM: not ready\n");
			return -EAGAIN;
		}
		/* rpm is tachometer frequency / 2 * 60 */
		*val = (ec_val * 60) >> 1;
		return 0;
	}
	case hwmon_fan_fault: {
		ret = loki_ec_read16(EC_FAN_RPM, &ec_val);
		if (ret)
			return ret;
		*val = ec_val == 0 ? 1 : 0;
		return 0;
	}
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_read_string_fan(struct device *dev, u32 attr, int channel,
				      const char **str)
{
	switch (attr) {
	case hwmon_fan_label:
		*str = LOKI_SYSTEM_FAN_LABEL;
		return 0;
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_read_string_temp(struct device *dev, u32 attr, int channel,
				      const char **str)
{
	switch (attr) {
	case hwmon_temp_label:
		*str = LOKI_SYSTEM_BAT_LABEL;
		return 0;
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_read_string(struct device *dev, enum hwmon_sensor_types type,
				  u32 attr, int channel, const char **str)
{
	switch (type) {
	case hwmon_fan:
		return loki_hwmon_read_string_fan(dev, attr, channel, str);
	case hwmon_temp:
		return loki_hwmon_read_string_temp(dev, attr, channel, str);
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_write_pwm(struct device *dev, enum hwmon_sensor_types type,
				u32 attr, int channel, long val)
{
	switch (type) {
	case hwmon_pwm:
		return ec_write(EC_FAN_DUTY, val);
	default:
		return -EINVAL;
	}
}

static umode_t loki_hwmon_is_visible(const void *data, enum hwmon_sensor_types type,
				     u32 attr, int channel)
{
	switch (type) {
	case hwmon_fan:
		return loki_hwmon_fan_is_visible(data, attr, channel);
	case hwmon_pwm:
		return loki_hwmon_pwm_is_visible(data, attr, channel);
	case hwmon_temp:
		return loki_hwmon_temp_is_visible(data, attr, channel);
	default:
		return 0;
	}
}

static int loki_hwmon_read(struct device *dev, enum hwmon_sensor_types type,
			   u32 attr, int channel, long *val)
{
	switch (type) {
	case hwmon_fan:
		return loki_hwmon_read_fan(dev, attr, channel, val);
	case hwmon_pwm:
		return loki_hwmon_read_pwm(dev, attr, channel, val);
	case hwmon_temp:
		return loki_hwmon_read_temp(dev, attr, channel, val);
	default:
		return -EINVAL;
	}
}

static int loki_hwmon_write(struct device *dev, enum hwmon_sensor_types type,
			    u32 attr, int channel, long val)
{
	switch (type) {
	case hwmon_fan:
		return -EOPNOTSUPP;
	case hwmon_pwm:
		return loki_hwmon_write_pwm(dev, type, attr, channel, val);
	case hwmon_temp:
		return -EOPNOTSUPP;
	default:
		return -EINVAL;
	}
}

static const u32 loki_hwmon_fan_config[] = {
	HWMON_F_INPUT | HWMON_F_FAULT | HWMON_F_LABEL,
	0
};

static const struct hwmon_channel_info loki_hwmon_fan = {
	.type	= hwmon_fan,
	.config = loki_hwmon_fan_config,
};

static const u32 loki_hwmon_pwm_config[] = {
	HWMON_PWM_INPUT,
	0
};

static const u32 loki_hwmon_bat_temp_config[] = {
	HWMON_T_INPUT | HWMON_T_MAX | HWMON_T_CRIT | HWMON_T_LABEL,
	0
};

static const struct hwmon_channel_info loki_hwmon_pwm = {
	.type	= hwmon_pwm,
	.config = loki_hwmon_pwm_config,
};

static const struct hwmon_channel_info loki_hwmon_bat_temp = {
	.type	= hwmon_temp,
	.config = loki_hwmon_bat_temp_config,
};

static const struct hwmon_channel_info *loki_hwmon_info[] = {
	&loki_hwmon_fan,
	&loki_hwmon_pwm,
        &loki_hwmon_bat_temp,
	NULL
};

static const struct hwmon_ops loki_hwmon_hwmon_ops = {
	.is_visible	= loki_hwmon_is_visible,
	.read		= loki_hwmon_read,
	.read_string	= loki_hwmon_read_string,
	.write		= loki_hwmon_write,
};

static const struct hwmon_chip_info loki_hwmon_chip_info = {
	.ops	= &loki_hwmon_hwmon_ops,
	.info	= loki_hwmon_info,
};

struct platform_device *pdev = NULL;

static int __init loki_hwmon_probe(struct platform_device *pdev)
{
	struct device *hwmon_dev;
	int rc = 0;

	hwmon_dev = devm_hwmon_device_register_with_info(&pdev->dev,
							 "loki_fan",
							 NULL,
							 &loki_hwmon_chip_info,
							 NULL);
	if (IS_ERR(hwmon_dev)) {
		rc = PTR_ERR(hwmon_dev);
		dev_err(&pdev->dev, "hwmon device register failed (%d)\n", rc);
		goto err_dev_reg;
	}

	return 0;

err_dev_reg:
	return rc;
}

static struct platform_driver loki_hwmon_driver = {
	.driver		= {
		.name	= "loki-hwmon",
	},
};

static __initdata struct dmi_system_id loki_hwmon_dmi_match[] = {
	{
		.ident = "DAQRI Compute Pack Developer Edition",
		.matches = {
			DMI_EXACT_MATCH(DMI_SYS_VENDOR, "DAQRI"),
			DMI_EXACT_MATCH(DMI_PRODUCT_NAME, "Compute Pack Developer Edition"),
		},
	},
	{ .ident = NULL }
};
MODULE_DEVICE_TABLE(dmi, loki_hwmon_dmi_match);

static int __init loki_hwmon_init(void)
{
	u8 major, minor, patch;
	int ret;

	if (!dmi_check_system(loki_hwmon_dmi_match)) {
		pr_err("Unsupported device\n");
		return -ENODEV;
	}

	if (ec_read(OEM_MAJOR_REV_RAM, &major) ||
	    ec_read(OEM_MINOR_REV_RAM, &minor) ||
	    ec_read(OEM_SUB_REV_RAM, &patch)) {
		pr_err("Error reading EC version\n");
		return -ENODEV;
	}
	if ((major < 2) ||
	    (major == 2 && minor < 3) ||
	    (major == 2 && minor == 3 && patch <= 11)) {
		pr_debug("EC version %d.%d.%d not supported\n", major, minor, patch);
		return -ENODEV;
	}

	pdev = platform_device_register_simple("loki-hwmon", -1, NULL, 0);
	if (IS_ERR(pdev)) {
		ret = PTR_ERR(pdev);
		goto err_device_reg;
	}

	ret = platform_driver_probe(&loki_hwmon_driver, loki_hwmon_probe);
	if (ret)
		goto err_driver_probe;

	return 0;

err_driver_probe:
	platform_device_unregister(pdev);
err_device_reg:
	return ret;
}

static void __exit loki_hwmon_exit(void)
{
	platform_driver_unregister(&loki_hwmon_driver);
	platform_device_unregister(pdev);
}

module_init(loki_hwmon_init);
module_exit(loki_hwmon_exit);

MODULE_AUTHOR("Alex Feinman <alex.feinman@daqri.com>");
MODULE_AUTHOR("Lucas Magasweran <lucas.magasweran@daqri.com>");
MODULE_DESCRIPTION("Loki EC sensors hwmon driver");
MODULE_VERSION("1.1.0");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:loki-hwmon");

